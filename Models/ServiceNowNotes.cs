﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace Snow_Integration.Models
{
        public class ServiceNowNotes
        {
            [JsonProperty(PropertyName = "new")]
            public string _new { get; set; }
            public string label { get; set; }
            public string sys_updated_on { get; set; }
            public string field { get; set; }
            public string sys_created_on { get; set; }
        }
}
