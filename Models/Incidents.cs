﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class Incidents
    {
        public Company company { get; set; }
        public string short_description { get; set; }
        public string u_preferred_contact_method { get; set; }
        public string number { get; set; }
        public string comments_and_work_notes { get; set; }
        public string u_affected_user_email { get; set; }
        public string approval_history { get; set; }
        public string comments { get; set; }
        public string approval_set { get; set; }
        public string due_date { get; set; }
        public string u_affected_user_phone { get; set; }
        public string work_notes_list { get; set; }
        public string upon_approval { get; set; }
        public string description { get; set; }
        public string sys_id { get; set; }
        public string approval { get; set; }
        public DateTime sys_updated_on { get; set; }
        public string work_notes { get; set; }
        public string u_affected_user_text { get; set; }
    }
}
