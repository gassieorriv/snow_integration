﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class Ticket
    {
        public static int id { get; set; }
        public static string recordType { get; set; }
        public static string siteName { get; set; }
        public static string city { get; set; }
    }
}
