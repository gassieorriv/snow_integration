﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class ConnectWise
    {
        private static bool isProd = false;
        public static string devApi = "https://api-staging.connectwisedev.com/v4_6_release/";
        public static object GetCompanies(string endpoint,string request)
        {
            ServiceLayer layer = new ServiceLayer();
            endpoint = endpoint + request;
            object value = layer.doGet(endpoint, request, new List<string>(), true, isProd,"","");
            return value;
        }

        public static object LookUpContact(string endpoint, string request)
        {
            ServiceLayer layer = new ServiceLayer();
          //  endpoint = endpoint + request;
            object value = layer.doGet(endpoint, request, new List<string>(), true, isProd, "", "");
            return value;
        }

        public static object CreateContact(string endpoint, string request,string body)
        {
            ServiceLayer layer = new ServiceLayer();
            endpoint = endpoint + request;
            object value = layer.doPost(endpoint, body, string.Empty, string.Empty, true,false);
            return value;
        }

        public static object GetTicket(string endpoint, string request)
        {
            ServiceLayer layer = new ServiceLayer();
            object value = layer.doGet(endpoint, request, new List<string>(), true, isProd, "", "");
            return value;
        }

        public static object CreateTicket(string endpoint,string request,string body)
        {
            ServiceLayer layer = new ServiceLayer();
            endpoint = endpoint + request;
            object value = layer.doPost(endpoint, body, string.Empty, string.Empty, true,false);
            return value;

        }
    }
}