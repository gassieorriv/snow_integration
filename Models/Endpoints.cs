﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class Endpoints
    {
        #region ServiceNow
        public static string GetServiceNowTicketByNumber(string number)
        {
            return "/api/now/table/incident?sysparm_query=number=" + number;
        }
        public static string CreateServiceNowTicketPullRequest(string assignmentGroup,int state)
        {
            //TODO: change to get users by state of ticket
            return "/api/now/table/incident?sysparm_query=assignment_group=" + assignmentGroup + "^state=" + 1;
        }
        public static string CreateServiceNowGetCommentsOrWorkNoteRequests(string commentOrWorknote, string id)
        {
            return "/api/now/table/sys_history_line?sysparm_query=set.id=" + id + "%5e" + "field=" + commentOrWorknote;
        }
        public static string GetCommentWorkNoteRequest(string id, string commentOrWorknote)
        {
          return "/api/now/table/sys_history_line?sysparm_query=set.id=" + id + " %5e" + "field=" + commentOrWorknote;
        }


        public static string GetExternalLink(string endpoint,string ticket)
        {
            return   endpoint + "nav_to.do?uri=incident.do?sys_id=" + ticket + "&sysparm_stack=incident_list.do&sysparm_query=active=true";
        }
        #endregion
        public static string CreateServiceNowGetCommentsOrWorkNoteRequests(string endpoint, string commentOrWorknote, string id)
        {
            return "/api/now/table/sys_history_line?sysparm_query=set.id=" + id + "%5e" + "field=" + commentOrWorknote;
        }
        #region ConnectWise
        public static string CreateConnectWiseGetIncidentRequest(string number)
        {
            return "apis/3.0/service/tickets?customFieldConditions=caption=\"Ext Ticket\" AND value = \"" + number + "\"";
        }
        public static string CreateConnectWiseGetByUpdatedTimeRequest()
        {
            return "apis/3.0/service/tickets?conditions=lastUpdated>" + "[" + DateTime.Today + "]";
        }
        public static string CreateConnectWiseNoteRequest(string id)
        {
            return "apis/3.0/service/tickets/" + id + "/notes";
        }
        public static string CreateConnectWiseTicketRequest()
        {
            return "apis/3.0/service/tickets";
        }
        public static string GetConnectWiseTicketNotes(string id)
        {
            return "apis/3.0/service/tickets/" + id + "/notes";
        }
        public static string GetConnectWiseTicketByDate()
        {
            return "apis/3.0/service/tickets?conditions=lastUpdated>[" + DateTime.Today.ToString("yyyy-MM-ddThh:mm:ssZ") + "]";
        }
        public static string GetConnectWiseCompanyConfiguration(string id)
        {
            return "apis/3.0/company/configurations?conditions=type/id=" + id + "&fields=questions";
        }
        public static string CreateContact()
        {
            return "apis/3.0/company/contacts";
        }
        public static string LookupContact(string email)
        {
            return "apis/3.0/company/contacts?childconditions=communicationItems/value like '" + email + "' AND communicationItems/communicationType='Email'";
        }
        public static string SaveContact()
        {
            return "apis/3.0/company/contacts";
        }
        #endregion
    }
}