﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
     public class ServiceNow
    {
        public static object GetIncidents(string endpoint,string username, string password)
        {
            ServiceLayer layer = new ServiceLayer();
            var value = layer.doGet(endpoint, "", new List<string>(), false, false, username, password);
            return value;
        }

        public static object GetNotesOrComments(string endpoint, string username, string password)
        {
            ServiceLayer layer = new ServiceLayer();
            var value = layer.doGet(endpoint, "", new List<string>(), false, false, username, password);
            return value;
        }
    }
}
