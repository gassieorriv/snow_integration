﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Net;
using System.IO;

namespace Snow_Integration.Models
{
    public class ServiceLayer
    {
        public static string privateKey { get { return ""; } }
        public static string publicKey { get { return ""; } }
        public static string company { get { return ""; } }
        public static string UATprivateKey { get { return "AiNnFiGfwqFud5xp"; } }
        public static string UATpublicKey { get { return "vkqCqtrqAdUw4ZYb"; } }
        public static string UATcompany { get { return "acuitytech_f"; } }
        public object doPost(string endpoint,string request,object Parameters,Dictionary<string,string> headerParams, bool hasHeaderParams, bool hasbasicAuthentication,bool isPROD,string username,string password)
        {
            var client = new RestClient(endpoint);
            var req = new RestRequest(request, Method.POST);
                req.AddJsonBody(Parameters);
          

            if (hasHeaderParams)
            {
                foreach(KeyValuePair<string,string> hp in headerParams)
                {
                    req.AddUrlSegment(hp.Key,hp.Value);
                }
            }

            if(hasbasicAuthentication)
            {
                byte[] plaintextbytes;
                if (isPROD)
                    plaintextbytes = System.Text.Encoding.UTF8.GetBytes(company +"+" + publicKey + ":" + privateKey);
                else
                    plaintextbytes = System.Text.Encoding.UTF8.GetBytes(UATcompany+ "+" +UATpublicKey+ ":" + UATprivateKey);
                String encoded = System.Convert.ToBase64String(plaintextbytes);
                 req.AddHeader("Authorization", "Bearer  " + encoded);
               
            }
            IRestResponse response = client.Execute(req);
            var content = response.Content;

            return content;

        }
        public object doGet(string endpoint, string request, List<string> Parameters, bool hasbasicAuthentication,bool isPROD,string username, string password)
        {
            var client = new RestClient(endpoint);
            var req = new RestRequest(request, Method.GET);
            foreach (string param in Parameters)
            {
                string[] p = param.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                req.AddParameter(p[0], p[1]);
            }

            if (hasbasicAuthentication)
            {
                byte[] plaintextbytes;
                if (isPROD)
                    plaintextbytes = System.Text.Encoding.UTF8.GetBytes(company + "+" + publicKey + ":" + privateKey);
                else
                    plaintextbytes = System.Text.Encoding.UTF8.GetBytes(UATcompany + "+" + UATpublicKey + ":" + UATprivateKey);
                String encoded = System.Convert.ToBase64String(plaintextbytes);
                req.AddHeader("Authorization", "Basic " + encoded);
            }
            
            if(username != string.Empty && password != string.Empty)
            {
               System.Net.NetworkCredential netCredential = new System.Net.NetworkCredential(username, password);
                req.Credentials = netCredential;
            }

            IRestResponse response = client.Execute(req);
            var content = response.Content;
            return content;
        }

        public object doPost(string URL, string body, string username, string password, bool hasBasicAuthorization,bool isPROD)
        {
            try
            {

                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(URL);
                request.Method = "POST";
                request.ContentType = "application/json";
                System.Net.NetworkCredential netCredential;
                byte[] plainTextBytes = null;

                if (username != string.Empty && password != string.Empty)
                {
                    netCredential = new System.Net.NetworkCredential(username, password);
                    request.Credentials = netCredential;
                }

                if (hasBasicAuthorization)
                {
                    byte[] plaintextbytes;
                    if (isPROD)
                        plaintextbytes = System.Text.Encoding.UTF8.GetBytes(company + "+" + publicKey + ":" + privateKey);
                    else
                        plaintextbytes = System.Text.Encoding.UTF8.GetBytes(UATcompany + "+" + UATpublicKey + ":" + UATprivateKey);
                    String encoded = System.Convert.ToBase64String(plaintextbytes);
                    request.Headers.Add("Authorization", "Basic " + encoded);
                }

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(body);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return streamReader.ReadToEnd();
                }
            }

            catch (Exception ex)
            {
                return false;
            }
        }

    }
}