﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Snow_Integration.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Snow_Integration.Models
{
    public class  Mapping
    {
       public static object MapSnowtoConnectWise(ConnectWise connectWise, ServiceNow snow)
        {
            var mappedParam = AutoMapper.Mapper.Map<ConnectWise>(snow);
            return mappedParam;
        }

        public static List<Companies> MapQuestitons(object configuration)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<JArray, Companies>();
            });

            JArray c = JArray.Parse(configuration.ToString());
            //IMapper mapper = config.CreateMapper();
            //return mapper.Map<List<Companies>>(c);
             return c.ToObject<List<Companies>>(JsonSerializer.Create());

        }


        public static List<ServiceNowNotes> MapServiceNowNotes(object notes)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<JArray, ServiceNowNotes>();
            });
            JObject t = JObject.Parse(notes.ToString());
            JArray tickets = JArray.Parse(t.First.First().ToString());
            return tickets.ToObject<List<ServiceNowNotes>>(JsonSerializer.Create());
        }

        public static Contact MapContact(object contact)
        {
            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<JArray, ServiceNowNotes>();
            });

            if (contact != null)
            {
                try
                {
                    JArray c = JArray.Parse(contact.ToString());
                    if (c.HasValues)
                    {
                        JObject j = JObject.Parse(c.First().ToString());
                        return j.ToObject<Contact>(JsonSerializer.Create());
                    }
                    else
                        return new Contact();

                }
                catch (Exception ex)
                {
                    JObject j = JObject.Parse(contact.ToString());
                    return j.ToObject<Contact>(JsonSerializer.Create());
                }
            }return new Contact();
        }

        public static Incidents MapIncident(object incident)
        {

            var config = new MapperConfiguration(cfg => {
                cfg.CreateMap<JArray, Incidents>();
            });
            JObject ticket = JObject.Parse(incident.ToString());
            return ticket.ToObject<Incidents>(JsonSerializer.Create());
        }

        public static List<Incidents> MapIncidents(object incident)
        {

            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<JArray, List<Incidents>>();
                });
                JObject ticket = JObject.Parse(incident.ToString());
                JArray t = JArray.Parse(ticket.First.First.ToString());
                return t.ToObject<List<Incidents>>(JsonSerializer.Create());
            }
            catch(Exception ex)
            {
          
            }
            return new List<Incidents>();
        }

        public static List<Ticket> MapTickets(object tickets)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<JArray, List<Ticket>>();
            });
            JArray t = JArray.Parse(tickets.ToString());
            return t.ToObject<List<Ticket>>(JsonSerializer.Create());

        }
    }
}