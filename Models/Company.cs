﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class Company
    {
        public string display_value { get; set; }
        public string link { get; set; }
    }
}
