﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class ConnectWiseNote
    {
        public int id { get; set; }
        public string ticketid { get; set; }
        public string text { get; set; }
        public string detailDescriptionFlag { get; set; }
        public string internalAnalysisFlag { get; set; }
        public string resolutionFlag { get; set; }
        public string dateCreated { get; set; }
        public bool internalFlag { get; set; }
        public bool externalFlag { get; set; }
    }
}
