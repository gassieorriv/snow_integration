﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snow_Integration.Models
{
    public class questions
    {
      public string answerId { get; set; }
      public string questionId { get; set; }
      public string question { get; set; }
      public string answer { get; set; }
      public string sequenceNumber { get; set; }
    }
}
