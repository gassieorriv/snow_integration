﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snow_Integration.Models;


namespace Snow_Integration.Jobs
{
    public class ConnectWiseConfiguration
    {
        
        public static object Companies()
        {
            return ConnectWise.GetCompanies(ConnectWise.devApi,Endpoints.GetConnectWiseCompanyConfiguration(24.ToString()));
        }
    }
}
