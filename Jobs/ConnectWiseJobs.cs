﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snow_Integration.Models;
using Newtonsoft.Json.Linq;

namespace Snow_Integration.Jobs
{
    public class ConnecctWiseJobs
    {

        static string endpoint { get; set; }
        static string username { get; set; }
        static string password { get; set; }
        static string assignment_group { get; set; }

        /// <summary>
        /// Creates tickets in ConnectWIse Based on Service Now Tickets
        /// TODO:   Implement Service board 
        /// Map     SLA parameters
        ///         Sync Connectwise Notes
        /// </summary>
        public static void CreateTickets()
        {
            //Get Configurations
            List<Companies> configurations = Mapping.MapQuestitons(Companies());
            foreach (Companies companyConfig in configurations)
            {
                assingConfigValues(companyConfig);
                string url = endpoint + Endpoints.CreateServiceNowTicketPullRequest(assignment_group, 1);
                List<Incidents> incidents = ServiceNowJobs.GetIncidents(url, username, password);
                foreach (Incidents incident in incidents)
                {
                    url = endpoint + Endpoints.GetCommentWorkNoteRequest(incident.sys_id, "worknotes");
                    List<ServiceNowNotes> worknotes = NoteJobs.GetServiceNowNotes(url, username, password);

                    url = endpoint + Endpoints.GetCommentWorkNoteRequest(incident.sys_id, "comments");
                    List<ServiceNowNotes> comments = NoteJobs.GetServiceNowNotes(url, username, password);

                    //Verify Contact
                    Contact contact = Mapping.MapContact(lookupContact(incident.u_affected_user_email));
                    
                    //if contact available use contact
                    if(contact.id <= 0)
                        contact = Mapping.MapContact(createContact("firstname", "lastname", incident.u_affected_user_email, incident.u_affected_user_phone));

                    //Verify Ticket
                    List<Ticket> number = Mapping.MapTickets(GetTicket(incident.number));
                    if (number.Count <= 0)
                    {
                        JObject ticket = null;
                        List<ServiceNowNotes> totalNotes = new List<ServiceNowNotes>();
                        totalNotes.AddRange(worknotes);
                        totalNotes.AddRange(comments);
                        object value = string.Empty;
                        totalNotes = totalNotes.OrderBy(x => x.sys_updated_on).ToList();
                        try
                        {
                            if (totalNotes.Count <= 0)   
                                value = CreateTicket(incident, endpoint, DateTime.Now.ToString()).ToString();
                            else
                                value = CreateTicket(incident, endpoint, totalNotes.Last().sys_created_on).ToString();
                            ticket = JObject.Parse(value.ToString());
                        }
                        catch(Exception ex) {  }
                     
                        if (ticket != null)
                        {
                          foreach(ServiceNowNotes note in totalNotes)
                            {
                                CreateConnectWiseNote(ticket.GetValue("id").ToString(),note._new, true, false, false, true, false, "Job", false, true);
                            }
                        }
                    }
                    else
                    {
                        //Get connect wise ticket
                        //Check the custom sync field to determine the last sync
                        //Chek the ServieNow ticket notes
                        // 
                    }
                }
            }
        }
        public static object Companies()
        {
            return ConnectWise.GetCompanies(ConnectWise.devApi, Endpoints.GetConnectWiseCompanyConfiguration(24.ToString()));
        }
        private static void assingConfigValues(Companies companyConfig)
        {
            endpoint = companyConfig.Questions.Select(x => x).Where(x => x.question.ToLower().Contains("base url")).FirstOrDefault().answer;
            username = companyConfig.Questions.Select(x => x).Where(x => x.question.ToLower().Contains("username")).FirstOrDefault().answer;
            password = companyConfig.Questions.Select(x => x).Where(x => x.question.ToLower().Contains("password")).FirstOrDefault().answer;
             assignment_group = "526bb621dbeabe0438f9fb541d961987"; //companyConfig.Questions.Select(x => x).Where(x => x.question.ToLower().Contains("password")).FirstOrDefault().answer;

        }
        private static object lookupContact(string email)
        {
            return ConnectWise.LookUpContact(ConnectWise.devApi, Endpoints.LookupContact(email));
        }
        private static object createContact(string firstname,string lastname, string email, string phone)
        {
            string body = "{";
            body += "'firstname':'" + firstname + "',";
            body += "'lastname':'" + lastname + "',"; 
            body += "'communicationItems':[{type: {'id':1},'value':'" + email + "'},{type : {'id':2},'value':'" + phone + "'}]";
            body += "}";
            return ConnectWise.CreateContact(ConnectWise.devApi, Endpoints.CreateContact(),body);
        }
        private static object GetTicket(string number)
        {
            return ConnectWise.GetTicket(ConnectWise.devApi, Endpoints.CreateConnectWiseGetIncidentRequest(number));
        }
        private static object CreateTicket(Incidents incident, string endpoint,string sync)
        {
            string initialDescription = "User Email: " + incident.u_affected_user_email + Environment.NewLine;
            initialDescription += "User Phone: " + incident.u_affected_user_phone + Environment.NewLine;
            initialDescription += "Preferred Contact Method: " + incident.u_preferred_contact_method + Environment.NewLine;
            initialDescription += Environment.NewLine + Environment.NewLine;
            initialDescription += incident.description;

            string body = string.Empty;
            body += "{\"summary\": \"" + incident.short_description + "\",";
            body += "\"initialDescription\":\"" + initialDescription + "\",";
            body += "\"poNumber\":\"" + incident.number + "\",";
            body += "\"Company\": { \"identifier\":\"microsoft\",\"name\":\"microsoft\"},";
            body += "\"customFields\": [{\"caption\":\"Ext Ticket\",\"id\":1,\"value\":\"" + incident.number + "\"},{\"caption\":\"Ext Tkt Link\",\"id\":2,\"value\":\"" + Endpoints.GetExternalLink(endpoint,incident.sys_id) + "\"},{\"caption\":\"SNOW Sync\",\"id\":3,\"value\":\"" + sync + "\"}],";
            body += "\"contactPhoneNumber\":\"" + incident.u_affected_user_phone + "\",";
            body += "\"contactEmailAddress\":\"" + incident.u_affected_user_email + "\",";
            body += "\"contactName\":\"" + incident.u_affected_user_text + "\"";          
            body += "}";
           return  ConnectWise.CreateTicket(ConnectWise.devApi, Endpoints.CreateConnectWiseTicketRequest(), body);
        }
        private static object CreateConnectWiseNote(string id, string text, bool detail, bool internl, bool resolution, bool customerupdated, bool process, string createdby, bool internl2, bool external)
        {
            string body = "{";
            body += "ticketId:\"" + id + "\",";
            body += "text:\"" + text + "\",";
            body += "detailDescriptionFlag:\"" + detail + "\",";
            body += "internalAnalysisFlag:\"" + internl + "\",";
            body += "resolutionFlag:\"" + resolution + "\",";
            body += "customerUpdatedFlag:\"" + customerupdated + "\",";
            body += "processNotifications:\"" + process + "\",";
            body += "createdBy:\"" + createdby + "\",";
            body += "internalFlag:\"" + internl2 + "\",";
            body += "externalFlag:\"" + external + "\",";
            body += "}";

            return ConnectWise.CreateTicket(ConnectWise.devApi, Endpoints.CreateConnectWiseNoteRequest(id), body);
        }
    }
}