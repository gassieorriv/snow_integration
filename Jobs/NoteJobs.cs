﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snow_Integration.Models;

namespace Snow_Integration.Jobs
{
    public class NoteJobs
    {
        public static List<ServiceNowNotes> GetServiceNowNotes(string endpoint, string username, string password)
        {
            return Mapping.MapServiceNowNotes(ServiceNow.GetNotesOrComments(endpoint, username, password));
        }
    }
}
