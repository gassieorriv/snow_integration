﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Snow_Integration.Models;

namespace Snow_Integration.Jobs
{
    public class ServiceNowJobs
    {
        public static List<Incidents> GetIncidents(string endpoint, string username, string password)
        {
            return Mapping.MapIncidents(ServiceNow.GetIncidents(endpoint, username, password));
        }
    }
}
